package com.example.movies.data.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


const val API_KEY = "51d97c6204ed56bb4fe07bd04c51705e"
//const val API_KEY = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI1MWQ5N2M2MjA0ZWQ1NmJiNGZlMDdiZDA0YzUxNzA1ZSIsInN1YiI6IjVlYTIxMGIxZWM0NTUyMDAyNzNkNTUxNiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.uSNav72CCjKwpb0XWpSxhaqOKXsdZeI7WvYeaYvIUbY"
const val BASE_URL = "https://api.themoviedb.org/3/"

const val POSTER_BASE_URL = "https://image.tmdb.org/t/p/w500/"

const val FIRST_PAGE = 1
const val POST_PER_PAGE = 20

private val loggingInterceptor = HttpLoggingInterceptor().apply {
    level = HttpLoggingInterceptor.Level.BODY

}

object TheMovieDbClient {

    fun getClient(): TheMovieDbI {
        val requestInterceptor = Interceptor {chain ->
            val url = chain.request()
                .url()
                .newBuilder()
                .addQueryParameter("api_key", API_KEY)
                .build()

            val request = chain.request()
                .newBuilder()
                .url(url)
                .build()

            return@Interceptor chain.proceed(request)
        }

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(requestInterceptor)
            .addInterceptor(loggingInterceptor)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build()

        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(TheMovieDbI::class.java)
    }
}