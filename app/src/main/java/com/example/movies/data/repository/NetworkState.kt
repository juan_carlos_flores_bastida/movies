package com.example.movies.data.repository


enum class Status {
    RUNNING,
    SUCCESS,
    FAILED,
    DISCONECTED,
    ENDOFLIST
}

class NetworkState(val status: Status, val msg: String) {

    companion object {
        val LOADED: NetworkState
        val LOADING: NetworkState
        val ERROR: NetworkState
        val WITH_OUT_INTERNET: NetworkState
        val ENDOFLIST: NetworkState

        init {
            LOADED = NetworkState(status = Status.SUCCESS, msg = "Success")
            LOADING = NetworkState(status = Status.RUNNING, msg = "Running")
            ERROR = NetworkState(status = Status.FAILED, msg = "Something went wrong!")
            WITH_OUT_INTERNET = NetworkState(status = Status.DISCONECTED, msg = "Check your internet connection")
            ENDOFLIST = NetworkState(status = Status.FAILED, msg = "You have reached the end")
        }
    }
}