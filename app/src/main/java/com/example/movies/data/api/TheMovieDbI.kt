package com.example.movies.data.api

import com.example.movies.data.vo.MovieDetails
import com.example.movies.data.vo.MoviesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TheMovieDbI {

    @GET("movie/popular")
    fun getMovies(@Query("page") page: Int): Single<MoviesResponse>

    @GET("movie/{movie_id}")
    fun getMovieDetails(@Path("movie_id")id: Int): Single<MovieDetails>
}