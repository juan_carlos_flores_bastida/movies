package com.example.movies.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.movies.data.api.TheMovieDbI
import com.example.movies.data.repository.NetworkState
import com.example.movies.data.vo.MovieDetails
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

class MovieDetailsNetworkDataSource (
    private val apiService: TheMovieDbI,
    private val compositeDisposable: CompositeDisposable
) {

    private val TAG = MovieDetailsNetworkDataSource::class.java.simpleName

    private val _networkState = MutableLiveData<NetworkState>()
    val networkState: LiveData<NetworkState>
    get() = _networkState

    private val _downloadMovieResponse = MutableLiveData<MovieDetails>()
    val downloadMovieResponse: LiveData<MovieDetails>
    get() = _downloadMovieResponse

    fun fetchMovieDetails(movieId: Int) {
        _networkState.postValue(NetworkState.LOADING)

        try {
            apiService.getMovieDetails(movieId)
                .subscribeOn(Schedulers.io())
                .subscribe(
                    { movieDetails ->
                        _downloadMovieResponse.postValue(movieDetails)
                        _networkState.postValue(NetworkState.LOADED)
                    },
                    {
                        _networkState.postValue(NetworkState.ERROR)
                        Log.e(TAG, it.message)
                    }
                )
        } catch (e: Exception) {
            Log.e(TAG, e.message)
        }
    }
}