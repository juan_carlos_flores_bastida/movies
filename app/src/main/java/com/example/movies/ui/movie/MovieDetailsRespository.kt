package com.example.movies.ui.movie

import androidx.lifecycle.LiveData
import com.example.movies.data.api.TheMovieDbI
import com.example.movies.data.repository.NetworkState
import com.example.movies.data.vo.MovieDetails
import com.example.movies.repository.MovieDetailsNetworkDataSource
import io.reactivex.disposables.CompositeDisposable

class MovieDetailsRespository(private val apiService: TheMovieDbI) {

    lateinit var movieDetailsNetworkDataSource: MovieDetailsNetworkDataSource

    fun fetchSingleMovieDetails(compositeDisposable: CompositeDisposable, movieId: Int): LiveData<MovieDetails> {
        movieDetailsNetworkDataSource = MovieDetailsNetworkDataSource(apiService, compositeDisposable)
        movieDetailsNetworkDataSource.fetchMovieDetails(movieId)

        return movieDetailsNetworkDataSource.downloadMovieResponse
    }

    fun getMovieDetailsNetworkState(): LiveData<NetworkState> {
        return movieDetailsNetworkDataSource.networkState
    }
}