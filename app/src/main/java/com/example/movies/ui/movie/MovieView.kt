package com.example.movies.ui.movie

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.movies.R
import com.example.movies.data.api.TheMovieDbClient
import com.example.movies.data.api.TheMovieDbI
import com.example.movies.data.repository.NetworkState
import com.example.movies.data.vo.MovieDetails

class MovieView : AppCompatActivity() {

    private lateinit var viewModel: SingleMovieViewModel
    private lateinit var movieRepository: MovieDetailsRespository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_view)

        val movieId = intent.getIntExtra("id", 1)
        //val movieId = 1

        val apiService: TheMovieDbI = TheMovieDbClient.getClient()
        movieRepository = MovieDetailsRespository(apiService)

        viewModel = getViewModel(movieId)

        viewModel.movieDetails.observe(this, Observer {
            bindUi(it)
        })

        viewModel.networkState.observe(this, Observer {
            if (it == NetworkState.LOADING)println("Muestra barra") else println("Oculta barra")
            if (it == NetworkState.ERROR) println("Muestra mensaje de error") else println("Oculta error")
        })
    }

    fun bindUi(md: MovieDetails) {
        println("Pelicula: $md")
    }

    private fun getViewModel(movieId: Int): SingleMovieViewModel {

        return ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return SingleMovieViewModel(movieRepository, movieId) as T
            }
        })[SingleMovieViewModel::class.java]
    }
}
